package nl.dutchland.temperaturelogger.sensors.temperatureandhumidity

typealias TemperatureListener = (TemperatureMeasurement) -> Unit

interface TemperatureSensor : InputDevice {
    fun getTemperature() : TemperatureMeasurement
}