package nl.dutchland.temperaturelogger.sensors.temperatureandhumidity

typealias HumidityListener = (HumidityMeasurement) -> Unit

interface HumiditySensor: InputDevice {
    fun getHumidity() : HumidityMeasurement
}