package nl.dutchland.temperaturelogger.sensors.temperatureandhumidity

interface InputDevice {
    fun start()
    fun stop()
}