package nl.dutchland.temperaturelogger.temperature

import org.ktorm.database.Database
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.*

object TemperatureTable : Table<TemperatureMeasurement>("temperature") {
    val id = int("id").primaryKey().bindTo { it.id }
    val timestamp = long("TIMESTAMP").bindTo { it.timestamp }
    val location = varchar("location").bindTo { it.location }
    val sensorType = varchar("sensor_type").bindTo { it.sensorType }
    val temperatureInKelvin = double("temperature_in_kelvin").bindTo { it.temperatureInKelvin }
}

val Database.temperatureTable get() = this.sequenceOf(TemperatureTable)