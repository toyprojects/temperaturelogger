package nl.dutchland.temperaturelogger.temperature

import org.ktorm.entity.Entity

interface HumidityMeasurement : Entity<HumidityMeasurement> {
    companion object : Entity.Factory<HumidityMeasurement>()
    val id: Int
    var timestamp: Long
    var location: String
    var sensorType: String
    var humidityPercentage: Double
}