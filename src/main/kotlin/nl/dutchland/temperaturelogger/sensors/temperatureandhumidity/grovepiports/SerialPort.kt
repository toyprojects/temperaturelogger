package nl.dutchland.temperaturelogger

interface SerialPort {
    val serialPort: String
}