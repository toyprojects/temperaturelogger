package nl.dutchland.temperaturelogger.sensors.temperatureandhumidity

import nl.dutchland.physics.baseunits.temperature.Temperature
import nl.dutchland.temperaturelogger.sensors.temperatureandhumidity.TimeStamp

data class TemperatureMeasurement(
        val temperature : Temperature,
        val timestamp: TimeStamp
)
