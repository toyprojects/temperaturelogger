package nl.dutchland.temperaturelogger

interface DigitalPort {
    val digitalPin: Int
}