package nl.dutchland.temperaturelogger

import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import java.sql.Connection

class DatabaseMigration(private val connection: Connection) {
    fun migrate() {
        val database: liquibase.database.Database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(connection))
        val liquibase = Liquibase("database/db.changelog-master.xml", ClassLoaderResourceAccessor(), database)
        liquibase.update(Contexts(), LabelExpression())
    }
}