package nl.dutchland.temperaturelogger.sensors.temperatureandhumidity

typealias TemperatureHumidityListener = (TemperatureHumidityMeasurement) -> Unit

interface TemperatureHumiditySensor : TemperatureSensor, HumiditySensor, InputDevice {
    fun getTemperatureHumidity() : TemperatureHumidityMeasurement
}