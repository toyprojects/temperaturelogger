import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    id("com.github.johnrengelman.shadow") version "5.2.0"
    application
}
group = "nl.dutchland.temperaturelogger"
version = "1.0-SNAPSHOT"

val nexusUserName: String by project
val nexusPassword: String by project

repositories {
    maven {
        url = uri("http://192.168.178.17:8081/repository/maven-public/")
        credentials {
            username = nexusUserName
            password = nexusPassword
        }
    }
}

dependencies {
    implementation("nl.dutchland.physics:physicsunit:0.0.1-20201215.155314-2")
    implementation("com.github.yafna.raspberry:grovepi-spec:0.1.1")
    implementation("com.github.yafna.raspberry:grovepi-pi4j:0.1.1")
    implementation("org.liquibase:liquibase-core:4.2.1")
    implementation("org.ktorm:ktorm-core:3.2.0")
    runtimeOnly("org.postgresql:postgresql:42.1.4")

    runtimeOnly("com.h2database:h2:1.4.200")
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.0")
    testImplementation(platform("org.junit:junit-bom:5.7.0"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

application {
//    mainClass.set("nl.dutchland.temperaturelogger.MainKt")
    mainClassName = "nl.dutchland.temperaturelogger.MainKt"
}

tasks.test {
    useJUnitPlatform()
}