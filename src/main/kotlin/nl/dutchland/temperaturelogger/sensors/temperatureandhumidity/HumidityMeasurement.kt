package nl.dutchland.temperaturelogger.sensors.temperatureandhumidity

import nl.dutchland.physics.RelativeHumidity
import nl.dutchland.temperaturelogger.sensors.temperatureandhumidity.TimeStamp

class HumidityMeasurement(
        val humidity : RelativeHumidity,
        val timestamp: TimeStamp
)