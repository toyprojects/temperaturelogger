package nl.dutchland.temperaturelogger.temperature

import org.ktorm.database.Database
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.*

object HumidityTable : Table<HumidityMeasurement>("humidity") {
    val id = int("id").primaryKey().bindTo { it.id }
    val timestamp = long("TIMESTAMP").bindTo { it.timestamp }
    val location = varchar("location").bindTo { it.location }
    val sensorType = varchar("sensor_type").bindTo { it.sensorType }
    val humidityPercentage = double("relative_humidity_percentage").bindTo { it.humidityPercentage }
}

val Database.humidityTable get() = this.sequenceOf(HumidityTable)