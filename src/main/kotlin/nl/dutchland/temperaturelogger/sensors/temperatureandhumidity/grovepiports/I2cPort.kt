package nl.dutchland.temperaturelogger

interface I2cPort {
    val i2cDeviceNumber: Int
}