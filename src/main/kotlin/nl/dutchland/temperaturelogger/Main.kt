package nl.dutchland.temperaturelogger

import com.github.yafna.raspberry.grovepi.pi4j.GrovePi4J
import com.pi4j.wiringpi.Gpio.delay
import nl.dutchland.physics.baseunits.temperature.Kelvin
import nl.dutchland.temperaturelogger.temperature.HumidityMeasurement
import nl.dutchland.temperaturelogger.temperature.TemperatureMeasurement
import nl.dutchland.temperaturelogger.temperature.humidityTable
import nl.dutchland.temperaturelogger.temperature.temperatureTable
import nl.dutchland.temperaturelogger.sensors.temperatureandhumidity.GroveTemperatureHumiditySensors.*
import nl.dutchland.temperaturelogger.sensors.temperatureandhumidity.TemperatureHumidityMeasurement
import nl.dutchland.temperaturelogger.sensors.temperatureandhumidity.TemperatureHumiditySensor
import nl.dutchland.temperaturelogger.sensors.temperatureandhumidity.TemperatureSensorBuilder
import org.ktorm.database.Database
import org.ktorm.entity.add
import org.ktorm.entity.toList
import org.ktorm.logging.ConsoleLogger
import org.ktorm.logging.LogLevel.INFO

//    private val database = Database.connect(
//            url = "jdbc:postgresql://192.168.178.12:5432/postgres",
//            logger = ConsoleLogger(threshold = INFO),
//            password = "mysecretpassword",
//            user = "postgres"
//    )
private val database = Database.connect(
        url = "jdbc:h2:mem:ktorm;DB_CLOSE_DELAY=-1",
        logger = ConsoleLogger(threshold = INFO))

private val grovePi = GrovePi4J();

private val sensor: TemperatureHumiditySensor = TemperatureSensorBuilder(grovePi)
        .onPort(GrovePiZero.A0)
        .withType(DHT11)
        .withListener { persist(it) }
        .build()

fun main() {
//    Temperature

    Runtime.getRuntime().addShutdownHook(object : Thread() {
        override fun run() {
            println("Gracefully shutting down")
            sensor.stop()
            grovePi.close()
            println("Gracefully shut!!!")
            delay(2000)
        }
    })


    database.migrate()

    println("Printing departments")
    for (measurement: TemperatureMeasurement in database.temperatureTable.toList()) {
        println(measurement.timestamp)
    }
}

fun Database.migrate() {
    this.useConnection { connection ->
        DatabaseMigration(connection)
                .migrate()
    }
}

fun persist(measurement: TemperatureHumidityMeasurement) {
    val temperatureMeasurement = TemperatureMeasurement {
        location = "Woonkamer"
        temperatureInKelvin = measurement.temperature.valueIn(Kelvin)
        timestamp = measurement.timeStamp.millisecondsSinceEpoch
        sensorType = DHT11.toString()
    }

    database.temperatureTable.add(temperatureMeasurement)

    val humidityMeasurement = HumidityMeasurement {
        location = "Woonkamer"
        humidityPercentage = measurement.humidity.relativeHumidity.percentage
        timestamp = measurement.timeStamp.millisecondsSinceEpoch
        sensorType = DHT11.toString()
    }

    database.humidityTable.add(humidityMeasurement)
}




