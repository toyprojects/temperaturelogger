package nl.dutchland.temperaturelogger

interface AnalogPort {
    val analogPin: Int
}