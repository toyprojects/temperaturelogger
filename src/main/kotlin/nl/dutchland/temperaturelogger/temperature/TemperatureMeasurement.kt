package nl.dutchland.temperaturelogger.temperature

import org.ktorm.entity.Entity

interface TemperatureMeasurement : Entity<TemperatureMeasurement> {
    companion object : Entity.Factory<TemperatureMeasurement>()
    val id: Int
    var timestamp: Long
    var location: String
    var sensorType: String
    var temperatureInKelvin: Double
}